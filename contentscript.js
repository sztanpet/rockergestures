window.addEventListener("mousedown", function( event ) {
  
  if ( event.which == 1 ) //leftmouse
    chrome.extension.sendRequest( {event: "LEFTMOUSEDOWN"} );
  else if( event.which == 3 ) //rightmouse
    chrome.extension.sendRequest( {event: "RIGHTMOUSEDOWN"} );
  
});

window.addEventListener("mouseup", function( event ) {
  
  if ( event.which == 1 ) //leftmouse
    chrome.extension.sendRequest( {event: "LEFTMOUSEUP"} );
  else if( event.which == 3 ) //rightmouse
    chrome.extension.sendRequest( {event: "RIGHTMOUSEUP"} );
  
});